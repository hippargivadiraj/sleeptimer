//
//  ContentView.swift
//  sleepTimer
//
//  Created by Leadconsultant on 11/14/19.
//  Copyright © 2019 Leadconsultant. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @State var sleepTime = 8.00
    @State var dateSelected = Date()
    
    var body: some View {
        VStack{
        Stepper(value: $sleepTime, in: 4...12, step: 0.25) {
            Text("\(sleepTime, specifier: "%g" ) hours")
        }
            DatePicker(selection: $dateSelected, in: Date()..., displayedComponents: .hourAndMinute) {
                Text("Pick The Dates")
            }.labelsHidden()
    }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
